#include<iostream>
#include<ctime>
#include<string>
#include<thread>
#include<chrono>
#include<rubberduk.hpp>

using namespace std;

int printSomething(int arg1, std::string arg2)
{
    cout << arg1 << ": " << "\""<< arg2 <<"\"" << endl;
    return 101;
}

int printSomething2(int arg1, std::string arg2)
{
    unsigned int eeg = arg1;
    return 432 + arg1;
}

int cnum(std::string str)
{
    std::cout << "\"" << str << "\"" << std::endl;
    return std::stoi(str) * 2;
}
int cnum3(std::string str)
{
    std::cout << "\"" << str << "\"" << std::endl;
    return std::stoi(str) * 3;
}
int main(int argc, char** argv)
{
    /**rubberduk::DukContext ctx;
    rubberduk::DukFunction f("cnum");
    rubberduk::DukFunction f1("cnum3");
    rubberduk::DukFunction f2("printSomething");
    rubberduk::DukFunction f3("printSomething2");

    f.bindFunction(cnum);
    f1.bindFunction(cnum3);
    f2.bindFunction(printSomething);
    f3.bindFunction(printSomething2);

    ctx.add(f);
    ctx.add(f1);
    ctx.add(f2);
    ctx.add(f3);

    string line = "";
    while(true)
    {
        cout << ">> ";
        getline(cin, line);
        if(line == "exit") break;
        else
        {
            ctx.eval(line);
        }
    }**/
    duk_context* ctx = duk_create_heap_default();
    string s;

    duk_dump_context_stdout(ctx);
    duk_push_int(ctx, 1);
    duk_dump_context_stdout(ctx);
    duk_push_int(ctx, 2);
    duk_dump_context_stdout(ctx);
    duk_push_int(ctx, 3);
    duk_dump_context_stdout(ctx);
    duk_pop(ctx);
    duk_dump_context_stdout(ctx);
    duk_pop(ctx);
    duk_dump_context_stdout(ctx);
    duk_pop(ctx);
    duk_dump_context_stdout(ctx);

    cin >> s;

    duk_destroy_heap(ctx);
    return 0;
}
