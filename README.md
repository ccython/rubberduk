# About RubberDuk #

RubberDuk is a C++ wrapper for the free and open source ECMAScript interpreter [duktape](http://duktape.org "duktape").

It aims to make a few things easier, such as exposing C++ classes to ECMAScript, and completely hiding the duktape C-API from the end user.

RubberDuk comes bundled with the duktape source files,  so you can one-shot-compile everything without additional dependencies.

RubberDuk itself only consists of one header, "rubberduk.hpp".

